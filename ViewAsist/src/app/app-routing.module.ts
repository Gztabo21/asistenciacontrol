import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormllgSldComponent } from './components/formllg-sld/formllg-sld.component';
import { LoginComponent } from './components/login/login.component';

const routes: Routes = [
  {
    path: '',
    component: FormllgSldComponent 
  },
  {
    path: 'login',
    component: LoginComponent 
  },{
    path: 'main',
    loadChildren: './components/dashboard/dashboard.module#DashBoardModule'
  },
  {
    path: '**',
    redirectTo: '',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
