import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MainComponent } from './components/main/main.component';
import { FormllgSldComponent } from './components/formllg-sld/formllg-sld.component';
import { RelojComponent } from './components/reloj/reloj.component';
//services
import {RelojService} from './reloj.service';
import { LoginComponent } from './components/login/login.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { EmployeeComponent } from './components/employee/employee.component';
import { SettingsComponent } from './components/settings/settings.component';
import { PermissionComponent } from './components/permission/permission.component';

@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    FormllgSldComponent,
    RelojComponent,
    LoginComponent,

  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [RelojService],
  bootstrap: [AppComponent]
})
export class AppModule { }
