import { NgModule }                 from '@angular/core';
import { RouterModule, Routes }     from '@angular/router';
import {DashboardComponent} from'./dashboard.component';
import { CommonModule } from '@angular/common';

const DashBoardRoutes: Routes = [

  
  {
    path: '',
    component: DashboardComponent,
    children: [
          {
            path: 'users',
            loadChildren: '../users/user.module#UsersModule'
          },

        {
            path: 'settings',
            loadChildren: '../settings/settings.module.ts#SettingsModule'
          }, 
          {
            path: 'permission',
            loadChildren: '../permission/permission.module.ts#PermissionModule'
          },
          {
            path: 'employe',
            loadChildren: '../employee/employee.module#EmployeeModule'
          },
           {
            path: '',
            redirectTo: '/main',
            pathMatch: 'full'
          } 
    ]
    
    
  },
  {
    path: '**',
    redirectTo: '/list',
    pathMatch: 'full'
  } 
  
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(DashBoardRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class DashBoardRoutingModule { }