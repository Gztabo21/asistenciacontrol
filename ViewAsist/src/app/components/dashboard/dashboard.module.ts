import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {DashBoardRoutingModule} from './dashboard-routing.module'
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import {DashboardComponent} from './dashboard.component';


@NgModule({
  declarations: [
    DashboardComponent

  ],
  
  imports: [
    CommonModule,
    DashBoardRoutingModule
  ],
  providers: [],
  bootstrap: [],
  
})
export class DashBoardModule { }
