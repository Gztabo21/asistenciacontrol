import { NgModule }                 from '@angular/core';
import { RouterModule, Routes }     from '@angular/router';

import { EmployeeComponent } from './employee.component'

const employeeRoutes: Routes = [
  {
    path: '',
    component: EmployeeComponent,
    children: [
        {
            path: 'list',
            component: EmployeeComponent,
          },
          {
            path: 'create',
            component: EmployeeComponent,
            data: { title: 'Usuarios' }
          },
          {
            path: 'update/:id',
            component: EmployeeComponent,
            data: { title: 'Usuarios' }
          },
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(employeeRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class EmployeeRoutingModule { }