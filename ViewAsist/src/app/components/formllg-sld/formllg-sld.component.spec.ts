import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormllgSldComponent } from './formllg-sld.component';

describe('FormllgSldComponent', () => {
  let component: FormllgSldComponent;
  let fixture: ComponentFixture<FormllgSldComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormllgSldComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormllgSldComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
