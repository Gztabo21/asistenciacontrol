import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {PermissionRoutingModule} from './permission.routing.module'
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { PermissionComponent } from './permission.component';


@NgModule({
  declarations: [
    PermissionComponent

  ],
  
  imports: [
    CommonModule,
    PermissionComponent
  ],
  providers: [],
  bootstrap: [],
  
})
export class PermissionModule { }