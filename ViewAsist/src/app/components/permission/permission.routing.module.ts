import { NgModule }                 from '@angular/core';
import { RouterModule, Routes }     from '@angular/router';

import { PermissionComponent } from './permission.component'

const permissionRoutes: Routes = [
  {
    path: '',
    component: PermissionComponent,
    children: [
        {
            path: 'list',
            component: PermissionComponent,
          },
          {
            path: 'create',
            component: PermissionComponent,
            data: { title: 'Usuarios' }
          },
          {
            path: 'update/:id',
            component: PermissionComponent,
            data: { title: 'Usuarios' }
          },
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(permissionRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class PermissionRoutingModule { }