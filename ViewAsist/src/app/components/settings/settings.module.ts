import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {SettingsRoutingModule} from './settings.routing.module'
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import {SettingsComponent} from './settings.component';


@NgModule({
  declarations: [
    SettingsComponent

  ],
  
  imports: [
    CommonModule,
    SettingsRoutingModule
  ],
  providers: [],
  bootstrap: [],
  
})
export class SettingsModule { }