import { NgModule }                 from '@angular/core';
import { RouterModule, Routes }     from '@angular/router';

import { SettingsComponent } from './settings.component'

const settingsRoutes: Routes = [
  {
    path: '',
    component: SettingsComponent,
    children: [
        {
            path: 'list',
            component: SettingsComponent,
          },
          {
            path: 'create',
            component: SettingsComponent,
            data: { title: 'Usuarios' }
          },
          {
            path: 'update/:id',
            component: SettingsComponent,
            data: { title: 'Usuarios' }
          },
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(settingsRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class SettingsRoutingModule { }