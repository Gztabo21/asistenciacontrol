import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {UserRoutingModule} from './user.routing.module'
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import {UsersComponent} from './users.component';
import { ListComponent } from './list/list.component';

@NgModule({
  declarations: [
    UsersComponent,
    ListComponent

  ],
  
  imports: [
    CommonModule,
    UserRoutingModule
  ],
  providers: [],
  bootstrap: [],
  
})
export class UsersModule { }