import { NgModule }                 from '@angular/core';
import { RouterModule, Routes }     from '@angular/router';

import { UsersComponent }            from './users.component';
import { ListComponent } from './list/list.component'

const userRoutes: Routes = [
  {
    path: '',
    component: UsersComponent,
    children: [
        {
            path: 'list',
            component: ListComponent,

          },
          {
            path: 'create',
            component: UsersComponent,
            data: { title: 'Usuarios' }
          },
          {
            path: 'update/:id',
            component: UsersComponent,
            data: { title: 'Usuarios' }
          },
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(userRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class UserRoutingModule { }